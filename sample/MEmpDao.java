// asmple/MEmpDao.java
import org.apache.ibatis.annotations.Param;
import java.util.Map;

public interface MEmpDao {
    public Map<String, Object> selectOne(
        @Param("empNo") int empNo
    );
}
