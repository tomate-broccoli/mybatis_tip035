// sample/Main.java
import java.io.Reader;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionFactory;
import java.util.Map;

public class Main {

   public static void main(String[] args) throws Exception {
       System.out.println("Hello, World!");

       Main main = new Main();
       main.mainProc();
   } 

   public void mainProc() throws Exception {
       Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
       // SqlSession session = new SqlSessionFactoryBuilder().build(reader).openSession();
       SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(reader);
       SqlSession session = factory.openSession();

       MEmpDao dao = session.getMapper(MEmpDao.class);
       Map<String, Object> map = dao.selectOne(5);
   }
}
